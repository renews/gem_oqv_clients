module OqvClients
  class Client < Base

    def all
      get_api("#{client_url}", access_token)
    end

    def get(client_id=nil)
      read_id = client_id || id
      get_api("#{client_url}/#{read_id}", access_token)
    end

    def get_remaining_credit_clients(token)
      get_api("#{client_url}/get_remaining_credit_clients", token)
    end

    def search(params)
      get_api("#{client_url}?#{params.to_param}", access_token)
    end

    def create(client)
      post_api("#{client_url}/signup", client)
    end

    def update(client, client_id=nil)
      read_id = client_id || id
      put_api("#{client_url}/#{read_id}", client, access_token)
    end

    def send_reset_instructions(client_id=nil, email)
      read_id = client_id || id
      post_api("#{client_url}/password", { client: { email: email } })
    end

    def send_to_abacos(client_id=nil)
      read_id = client_id || id
      post_api("#{client_url}/#{read_id}/send_to_abacos", {}, access_token)
    end

    def addresses(client_id=nil)
      read_id = client_id || id
      get_api("#{client_url}/#{read_id}/addresses", access_token)
    end

    def get_devices(client_id = nil)
      read_id = client_id || id
      get_api("#{client_url}/#{read_id}/devices", access_token)
    end

    def is_token_valid?(token=nil)
      verify_token = token || access_token
      begin
        { client: post_api("#{client_url}/authenticate", nil, verify_token), valid: true }
      rescue
        { client: nil, valid: false }
      end
    end

  end
end
