module OqvClients
  class Base

    attr_accessor :config, :access_token, :id, :name, :email, :admin

    def initialize(config = OqvClients.configuration)
      @config = config
    end

    def get_api(url, access_token)
      response = RestClient.get url, { "X-OQvestir-Token" => access_token }
      JSON.parse(response)
    end

    def post_api(url, payload, access_token=nil)
      headers = { content_type: 'application/json' }
      headers.merge!({ "X-OQvestir-Token" => access_token }) if access_token.present?
      response = RestClient.post url, payload.to_json, headers
      JSON.parse(response)
    end

    def put_api(url, payload, access_token)
      response = RestClient.put url, payload.to_json, { content_type: 'application/json', "X-OQvestir-Token" => access_token }
      JSON.parse(response)
    end

    def delete_api(url, access_token)
      response = RestClient.delete url, { content_type: 'application/json', "X-OQvestir-Token" => access_token }
      JSON.parse(response) rescue nil
    end

    def generate_access_token!(email=nil, password=nil)
      client_email = email || config.client_email_api
      client_password = password || config.client_password_api
      response = post_api("#{client_url}/login", { email: client_email, password: client_password })
      @id = response["client"]["id"]
      @name = response["client"]["name"]
      @email = response["client"]["email"]
      @access_token = response["auth_token"]
    end

    def login_by_token(token)
      client = post_api("#{client_url}/authenticate", nil, token)
      @id = client['id']
      @access_token = token
      @name = client['name']
      @email = client['email']
      @admin = client['admin']
      self
    rescue
      nil
    end

    def client_url
      "#{config.client_api_url}/clients"
    end

  end
end
