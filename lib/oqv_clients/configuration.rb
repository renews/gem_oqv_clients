module OqvClients
  class Configuration
    attr_accessor :client_email_api, :client_password_api, :client_api_url

    def initialize
      @client_email_api = client_email_api || ENV['CLIENT_EMAIL_API']
      @client_password_api = client_password_api|| ENV['CLIENT_PASSWORD_API']
      @client_api_url = client_api_url|| ENV['CLIENT_API_URL'] || 'http://localhost:3000/api/v1'
    end
  end
end